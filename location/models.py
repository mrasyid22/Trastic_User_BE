from ctypes import addressof
from django.db import models
from authentication.models import Alamat, User, Pengguna, Warung, phoneModel
from exchange.models import Transaction
import random
from django.utils import timezone
from datetime import datetime

class Location(models.Model):
    warung = models.ForeignKey(to=Warung, default=None, blank=True, null=True, on_delete=models.CASCADE)
    latitude = models.CharField(max_length=30)
    longitude = models.CharField(max_length=30)
    address = models.CharField(max_length=100)


    def __str__(self):
        return str(self.warung.name+ " - "+ str(self.address))
