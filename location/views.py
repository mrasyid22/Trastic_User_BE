from django.shortcuts import render
from rest_framework.generics import ListAPIView, RetrieveAPIView,UpdateAPIView, CreateAPIView
from .models import Alamat, Location, User, Pengguna, phoneModel, Warung
from .serializers import ListLocationSerializer
from rest_framework.response import Response

class LocationListView(ListAPIView):
    
    serializer_class = ListLocationSerializer

    def get_queryset(self):
        location = Location.objects.all()
        return location

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            serializer_data = serializer.data
            sorted_serializer_data = sorted(serializer_data, key=lambda x: x['distance'])
            return self.get_paginated_response(sorted_serializer_data)

        serializer = self.get_serializer(queryset, many=True)
        serializer_data = serializer.data
        sorted_serializer_data = sorted(serializer_data, key=lambda x: x['distance'])
        return Response(sorted_serializer_data)