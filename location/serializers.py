from pyexpat import model
from rest_framework import serializers
from .models import Location
from math import sin, cos, sqrt, atan2, radians
from authentication.models import Warung


class ListLocationSerializer(serializers.ModelSerializer):
    distance = serializers.SerializerMethodField('get_distance')
    name = serializers.SerializerMethodField('get_warung_name')
    def get_distance(self, location):
        
        latitude_from = radians(float(self.context['request'].GET.get('latitude')))
        longitude_from = radians(float(self.context['request'].GET.get('longitude')))
        
        # latitude_from = radians(-6.217197336083227)
        # longitude_from = radians(107.01649427096568)
        
        latitude_to = radians(float(location.latitude))
        longitude_to = radians(float(location.longitude))
        
        dlon = longitude_to - longitude_from
        dlat = latitude_to - latitude_from
        # approximate radius of earth in km
        R = 6373.0
        
        a = sin(dlat / 2)**2 + cos(latitude_from) * cos(latitude_to) * sin(dlon / 2)**2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))

        distance = R * c
        distance_rounded = round(distance, 2)
        return distance_rounded
    
    def get_warung_name(self, location):
        name = location.warung.name
        return name
    
    class Meta:
        model = Location
        ordering = ['-distance']
        fields = ["name", "latitude", "longitude", "address", "distance"]
    