from django.urls import path, re_path
from .views import TransferPointsView, WithdrawPointsView
app_name = "exchange"
urlpatterns = [
    path('transfer/', TransferPointsView.as_view(),name='transfer'),
    path('withdraw/', WithdrawPointsView.as_view(),name='withdraw')
]