# Generated by Django 3.1.7 on 2022-04-25 03:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('points', '0005_withdrawpoints_phone'),
    ]

    operations = [
        migrations.AlterField(
            model_name='withdrawpoints',
            name='method',
            field=models.CharField(blank=True, choices=[('DANA', 'DANA'), ('OVO', 'OVO'), ('Gopay', 'Gopay'), ('ShopeePay', 'ShopeePay'), ('LinkAja', 'LinkAja')], default='DANA', max_length=30),
        ),
    ]
