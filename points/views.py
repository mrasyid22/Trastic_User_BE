from django.shortcuts import render

from http.client import NOT_FOUND
from django.shortcuts import render
import os
from authentication.models import Alamat, User, Pengguna, Warung
from exchange.models import Transaction
from exchange.permissions import IsPengguna, IsWarung
from .models import TransferPoints
from rest_framework.views import APIView
from rest_framework import generics, status, views, permissions, viewsets
from rest_framework.generics import ListAPIView, RetrieveAPIView, UpdateAPIView, CreateAPIView
from .serializers import TransferPointsSerializer, WithdrawPointsSerializer
from django.http import HttpResponsePermanentRedirect
from rest_framework.response import Response
from django.urls import reverse
import jwt
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
import pyotp
from rest_framework.response import Response
from rest_framework.views import APIView

class TransferPointsView(CreateAPIView):
    serializer_class = TransferPointsSerializer
    permission_classes = (permissions.IsAuthenticated,IsPengguna)

    def perform_create(self, serializer):
        pengguna = Pengguna.objects.get(user=self.request.user)
        if(int(pengguna.poin) >= int(self.request.data["total_poin"])):
            transaksi = Transaction.objects.create(type="Transfer", pengguna=pengguna)
        
            warung_id = self.request.data["warung_id"]
            warung = Warung.objects.get(custom_id=warung_id)
            serializer_data =  serializer.save(warung=warung,pengguna=pengguna, transaksi= transaksi, is_confirmed = True)
        return serializer_data

class WithdrawPointsView(CreateAPIView):
    serializer_class = WithdrawPointsSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        if self.request.user.is_pengguna:
            pengguna = Pengguna.objects.get(user=self.request.user)
            if(int(pengguna.poin) >= int(self.request.data["total_poin"])):
                transaksi = Transaction.objects.create(type="Withdraw", pengguna=pengguna)
            
                serializer_data =  serializer.save(pengguna=pengguna, transaksi= transaksi, is_confirmed = False)
        if self.request.user.is_warung:
            warung = Warung.objects.get(user=self.request.user)
            if(int(warung.poin) >= int(self.request.data["total_poin"])):
                transaksi = Transaction.objects.create(type="Withdraw", warung=warung)
            
                serializer_data =  serializer.save(warung=warung, transaksi= transaksi, is_confirmed = False)
        
        return serializer_data
