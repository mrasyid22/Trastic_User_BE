from pyexpat import model
from rest_framework import serializers
from .models import TransferPoints, WithdrawPoints


class TransferPointsSerializer(serializers.ModelSerializer):

    class Meta:
        model = TransferPoints
        fields = ["pengguna_id", "transaksi_id", "warung_id", "total_poin","is_confirmed"]
        
class WithdrawPointsSerializer(serializers.ModelSerializer):

    class Meta:
        model = WithdrawPoints
        fields = ["phone", "pengguna_id", "warung_id", "transaksi_id", "total_poin", "method"]