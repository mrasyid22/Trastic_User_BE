from django.db import models
from authentication.models import Alamat, User, Pengguna, Warung, phoneModel
from exchange.models import Transaction
import random
from django.utils import timezone
from datetime import datetime

WITHDRAW_METHOD_CHOICES = (
    ("DANA", "DANA"),
    ("OVO", "OVO"),
    ("Gopay", "Gopay"),
    ("ShopeePay", "ShopeePay"),
    ("LinkAja","LinkAja")
)

class TransferPoints(models.Model):
    custom_id = models.CharField(max_length=255, primary_key=True)
    pengguna = models.ForeignKey(to=Pengguna, default=None, blank=True, null=True, on_delete=models.CASCADE)
    transaksi = models.ForeignKey(to=Transaction, default=None, blank=True, null=True, on_delete=models.CASCADE)
    total_poin = models.IntegerField(default=0, blank=False)
    warung = models.ForeignKey(to=Warung, default=None, blank=True, null=True, on_delete=models.CASCADE)
    is_confirmed = models.BooleanField(default=True)
    
    def __str__(self):
        return str(self.custom_id+ " - "+ str(self.total_poin))

    def save(self,*args, **kwargs):
        
        if not self.custom_id:
            prefix = 'TF{}'.format(timezone.now().strftime('%y%m%d'))
            prev_instances = self.__class__.objects.filter(custom_id__contains=prefix)
            if prev_instances.exists():
                last_instance_id = prev_instances.last().custom_id[-4:]
                self.custom_id = prefix+'{0:04d}'.format(int(last_instance_id)+1)
            else:
                self.custom_id = prefix+'{0:04d}'.format(1)
                
        if self.is_confirmed:
            self.transaksi.status = "Selesai"
            self.warung.poin += self.total_poin
            self.transaksi.total_poin = self.total_poin
            self.pengguna.poin -= self.total_poin
            self.warung.save()
            self.pengguna.save()
            self.transaksi.save()
            
        super(TransferPoints, self).save(*args, **kwargs)

class WithdrawPoints(models.Model):
    custom_id = models.CharField(max_length=255, primary_key=True)
    pengguna = models.ForeignKey(to=Pengguna, default=None, blank=True, null=True, on_delete=models.CASCADE)
    warung = models.ForeignKey(to=Warung, default=None, blank=True, null=True, on_delete=models.CASCADE)
    transaksi = models.ForeignKey(to=Transaction, default=None, blank=True, null=True, on_delete=models.CASCADE)
    total_poin = models.IntegerField(default=0, blank=False)
    phone = models.CharField(blank=False, max_length=20, default=0)
    is_confirmed = models.BooleanField(default=False)
    method = models.CharField(max_length=30, choices=WITHDRAW_METHOD_CHOICES, default=WITHDRAW_METHOD_CHOICES[0][0] ,blank=True)
    pending_poin = models.IntegerField(default=0, blank=False)
    
    def __str__(self):
        return str(self.custom_id+ " - "+ str(self.total_poin))

    def save(self,*args, **kwargs):
        self.pending_poin += self.total_poin
        if self.pengguna:
            self.pengguna.poin -= self.total_poin
        if self.warung:
            self.warung.poin -= self.total_poin
        
        if not self.custom_id:
            prefix = 'WD{}'.format(timezone.now().strftime('%y%m%d'))
            prev_instances = self.__class__.objects.filter(custom_id__contains=prefix)
            if prev_instances.exists():
                last_instance_id = prev_instances.last().custom_id[-4:]
                self.custom_id = prefix+'{0:04d}'.format(int(last_instance_id)+1)
            else:
                self.custom_id = prefix+'{0:04d}'.format(1)
                
        if self.is_confirmed:
            self.transaksi.status = "Selesai"
            self.transaksi.total_poin = self.total_poin
            self.pending_poin =0
            
            if self.pengguna:
                self.pengguna.save()
            if self.warung:
                self.warung.save()
            self.transaksi.save()
            
        super(WithdrawPoints, self).save(*args, **kwargs)
