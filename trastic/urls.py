from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', include('authentication.urls', namespace="authentication")),
    path('exchange/', include('exchange.urls', namespace="exchange")),
    path('points/', include('points.urls', namespace="points")),
    path('location/', include('location.urls', namespace="location")),
    path('achievement/', include('achievement.urls', namespace="achievement")),
    path('kabar/', include('kabar.urls', namespace="kabar"))
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
