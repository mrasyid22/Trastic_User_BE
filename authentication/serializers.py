from dataclasses import field
from webbrowser import get
from rest_framework import serializers
from .models import Alamat, User, Pengguna, phoneModel, Warung
from django.contrib import auth
from rest_framework.exceptions import AuthenticationFailed, PermissionDenied
from rest_framework_simplejwt.tokens import RefreshToken, TokenError
from django.utils.encoding import smart_str, force_str, smart_bytes, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode

from django.shortcuts import get_object_or_404
from achievement.models import Achievement, Stage1Achievement, Stage2Achievement,Stage3Achievement,Stage4Achievement

class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        max_length=68, min_length=6, write_only=True)

    class Meta:
        model = User
        fields = ["id", "email", "password"]


class RegisterPenggunaSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Pengguna
        fields = ["user", "name", "phone",
                  "profile_img", "city"]

    def validate(self, attrs):
        email = attrs.get('email', '')
        phone_number = attrs.get('phone', '')
        if phoneModel.objects.filter(number=phone_number).exists():
            raise PermissionDenied({"message": 'No HP telah terdaftar'})
        
        return attrs

    def create(self, validation_data):
        user_data = validation_data.pop('user')
        phone_number = validation_data.pop('phone')
        user_obj = User.objects.create_user(**user_data)
        user_obj.is_pengguna = True
        user_obj.save()
        pengguna = Pengguna.objects.create(user=user_obj,phone=phone_number,  **validation_data)
        phone_model_user = phoneModel.objects.create(pengguna=pengguna, number=phone_number)
        stage1 = Stage1Achievement.objects.create()
        stage2 = Stage2Achievement.objects.create()
        stage3 = Stage3Achievement.objects.create()
        stage4 = Stage4Achievement.objects.create()
        achievement = Achievement.objects.create(pengguna=pengguna, stage1 = stage1,stage2 = stage2, stage3 = stage3, stage4 = stage4)
        
        return pengguna

class RegisterWarungSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Warung
        fields = ["user", "name", "phone",
                  "alamat", "city"]

    def validate(self, attrs):
        email = attrs.get('email', '')
        phone_number = attrs.get('phone', '')
        if phoneModel.objects.filter(number=phone_number).exists():
            phone_model = phoneModel.objects.get(number=phone_number)
            if(phone_model.warung != None):
                raise PermissionDenied({"message": 'No HP telah terdaftar'})
        
        return attrs

    def create(self, validation_data):
        user_data = validation_data.pop('user')
        phone_number = validation_data.pop('phone')
        user_obj = User.objects.create_user(**user_data)
        user_obj.is_warung = True
        user_obj.save()
        warung = Warung.objects.create(user=user_obj,phone=phone_number,  **validation_data)
        if phoneModel.objects.filter(number=phone_number).exists():
            phone_model = phoneModel.objects.get(number=phone_number)
            phone_model.warung = warung
            phone_model.save()
        else:
            phoneModel.objects.create(pengguna = None ,warung=warung, number=phone_number)
        
        return warung


class EmailVerificationSerializer(serializers.ModelSerializer):
    token = serializers.CharField(max_length=555)

    class Meta:
        model = phoneModel
        fields = ["phone", "otp"]

class LoginSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(max_length=255)
    password = serializers.CharField(min_length=6, write_only=True)
    tokens = serializers.EmailField(max_length=255, read_only=True)

    class Meta:
        model = User
        fields = ['email', 'password', 'tokens']

    def validate(self, attrs):
        email = attrs.get('email', '')
        password = attrs.get('password', '')
        user = auth.authenticate(email=email, password=password)
        if not user:
            raise AuthenticationFailed({"message": 'Email atau kata sandi salah. Silakan coba lagi.'})

        if not user.is_verified:
            if user.is_warung:
                phone = Warung.objects.get(user=user).phone
            if user.is_pengguna:
                phone = Pengguna.objects.get(user=user).phone
            raise AuthenticationFailed({
                "message": 'Email belum terverifikasi. Silakan verifikasi email Anda',
                "phone": phone
                })

        return {
            "email": user.email,
            "tokens": user.tokens
        }
        
class LogoutSerializer(serializers.Serializer):
    refresh = serializers.CharField()

    def validate(self, attrs):
        self.token = attrs['refresh']
        return attrs

    def save(self, **kwargs):
        try:
            RefreshToken(self.token).blacklist()

        except Exception as e:
            raise AuthenticationFailed({'message': 'Token salah'}, 401)
           
class ResetPasswordEmailRequestSerializer(serializers.Serializer):
    email = serializers.EmailField(min_length=2)

    class Meta:
        fields = ['email']

class SetNewPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(
        min_length=8, max_length=68, write_only=True)
    email = serializers.CharField(
        min_length=5, write_only=True)

    class Meta:
        fields = ['password', 'email']

    def validate(self, attrs):
        try:
            password = attrs.get('password')
            email = attrs.get('email')

            user = User.objects.get(email=email)
            user.set_password(password)
            user.save()

            return (user)
        except Exception as e:
            raise AuthenticationFailed({'message': 'Akun tidak ditemukan'}, 401)

class UserDetailSerializer(serializers.Serializer):
    email = serializers.EmailField(min_length=2)
    tokens = serializers.EmailField(max_length=255, read_only=True)

    class Meta:
        model = User
        fields = ['email', 'tokens']
        
class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

    class Meta:
        fields = ['old_password', 'new_password']

class AlamatSerializer(serializers.ModelSerializer):

    class Meta:
        model = Alamat
        fields = ['id','nama', 'phone', 'alamat', 'is_main']

