from django.core.mail import EmailMessage

import threading


class EmailThread(threading.Thread):

    def __init__(self, email):
        self.email = email
        threading.Thread.__init__(self)

    def run(self):
        self.email.send()


class Util:
    @staticmethod
    def send_email_pengguna(data):
        email_body = f'Hallo Trasticers {data["pengguna"]}, Gunakan kode OTP di bawah ini untuk verifikasi akun Trastic kamu \n\n {data["otp"]}'
        email_subject = "Verifikasi Akun Trastic"

        email = EmailMessage(
            subject=email_subject, body=email_body, to=[data["target_email"]]
        )
        email.send()
    
    def send_email_forget_password(data):
        email_body = f'Hallo Trasticers {data["pengguna"]}, Gunakan kode OTP di bawah ini untuk reset kata sandi akun Trastic kamu \n\n {data["otp"]}'
        email_subject = "OTP reset kata sandi Akun Trastic"

        email = EmailMessage(
            subject=email_subject, body=email_body, to=[data["target_email"]]
        )
        email.send()
