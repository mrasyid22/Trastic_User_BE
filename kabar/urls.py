from django.urls import path, re_path
from .views import CreateKabarView, ListKabarView

app_name = "kabar"
urlpatterns = [
    path('list-kabar/', ListKabarView.as_view(),name='list-kabar'),
]