from pyexpat import model
from rest_framework import serializers
from .models import Kabar


class KabarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kabar
        fields = ["title", "date", "thumbnail", "text"]