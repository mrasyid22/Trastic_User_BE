from cgitb import text
from django.db import models
from django.utils import timezone
from datetime import datetime

def upload_kabar_path(instance, filename):
    return "/".join(["kabar", filename])

class Kabar(models.Model):
    title = models.CharField(max_length=255, primary_key=True)
    date = models.DateTimeField(auto_now_add=True,editable=False)
    text = models.TextField()
    thumbnail = models.ImageField(
        blank=True, null=True, upload_to=upload_kabar_path)

    def __str__(self):
        return str(self.title+ " - "+ str(self.date))

