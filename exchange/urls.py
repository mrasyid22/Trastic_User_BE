from django.urls import path, re_path
from .views import DetailTransactionView, ExchangeSatuanView, TransactionListView, UpdateExchangeSatuanView, ExchangePaketanView, ExchangePenjemputanView, ComissionAndStatisticsWarungView, PendingExchangeSatuanListView

app_name = "exchange"
urlpatterns = [
    path('exchange-satuan/', ExchangeSatuanView.as_view(),name='exchange-satuan'),
    re_path('^exchange-satuan/update/(?P<custom_id>\w+)$',UpdateExchangeSatuanView.as_view(), name='update exchange satuan'),
    path('exchange-paketan/', ExchangePaketanView.as_view(),name='exchange-paketan'),
    path('exchange-penjemputan/', ExchangePenjemputanView.as_view(),name='exchange-penjemputan'),
    path('transactions/', TransactionListView.as_view(), name="transactions"),
    path('transaction/detail/<custom_id>', DetailTransactionView.as_view(), name="detail transactions"),
    path('warung-statistics/', ComissionAndStatisticsWarungView.as_view(),name='warung-commission'),
    path('pending-exchange-satuan/', PendingExchangeSatuanListView.as_view(),name='pending-satuan')
]