# Generated by Django 3.1.7 on 2022-04-25 01:48

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('exchange', '0009_exchangepenjemputan'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exchangepenjemputan',
            name='date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
