from django.db import models
from authentication.models import Alamat, User, Pengguna, Warung, phoneModel
import random
from django.utils import timezone
from datetime import datetime

    
TRANSACTION_STATUS_CHOICES = (
    ("Menunggu Konfirmasi", "Menunggu Konfirmasi"),
    ("Gagal", "Gagal"),
    ("Selesai", "Selesai")
)

TRANSACTION_TYPE_CHOICES = (
    ("Satuan", "Satuan"),
    ("Paketan", "Paketan"),
    ("Penjemputan", "Penjemputan"),
    ("Transfer", "Transfer"),
    ("Withdraw","Withdraw"),
    ("Reward", "Reward")
)

class Transaction(models.Model):
    custom_id = models.CharField(max_length=255, primary_key=True)
    total_poin = models.IntegerField(default=0, blank=False)
    pengguna = models.ForeignKey(to=Pengguna, default=None, blank=True, null=True, on_delete=models.CASCADE)
    warung = models.ForeignKey(to=Warung, default=None, blank=True, null=True, on_delete=models.CASCADE)
    status = models.CharField(max_length=30, choices=TRANSACTION_STATUS_CHOICES, default=TRANSACTION_STATUS_CHOICES[0][0] ,blank=True)
    type = models.CharField(max_length=30,choices=TRANSACTION_TYPE_CHOICES, default=TRANSACTION_TYPE_CHOICES[0][0] ,blank=True)
    date = models.DateTimeField(auto_now_add=True, blank=True)
    
    def __str__(self):
        return str(self.custom_id)
    
    def save(self,*args, **kwargs):
        self.date = timezone.now()
        if not self.custom_id:
            prefix = 'TRX{}'.format(timezone.now().strftime('%y%m%d'))
            prev_instances = self.__class__.objects.filter(custom_id__contains=prefix)
            if prev_instances.exists():
                last_instance_id = prev_instances.last().custom_id[-4:]
                self.custom_id = prefix+'{0:04d}'.format(int(last_instance_id)+1)
            else:
                self.custom_id = prefix+'{0:04d}'.format(1)
        super(Transaction, self).save(*args, **kwargs)
 

class ExchangeSatuan(models.Model):
    custom_id = models.CharField(max_length=255, primary_key=True)
    pengguna = models.ForeignKey(to=Pengguna, default=None, blank=True, null=True, on_delete=models.CASCADE)
    transaksi = models.ForeignKey(to=Transaction, default=None, blank=True, null=True, on_delete=models.CASCADE)
    warung = models.ForeignKey(to=Warung, null=True, default=None, blank=True, on_delete=models.CASCADE)
    transaksi_warung = models.ForeignKey(to=Transaction, default=None, blank=True, null=True, on_delete=models.CASCADE, related_name='transaksi_warung_satuan')
    botol_besar = models.IntegerField(default=0, blank=False)
    botol_sedang = models.IntegerField(default=0, blank=False) 
    botol_kecil = models.IntegerField(default=0, blank=False)
    gelas_besar = models.IntegerField(default=0, blank=False)
    gelas_kecil = models.IntegerField(default=0, blank=False)
    total_poin = models.IntegerField(default=0, blank=False)
    date = models.DateTimeField(auto_now_add=True,editable=False)
    is_confirmed = models.BooleanField(default=False)

    def __str__(self):
        return str(self.custom_id)

    def save(self,*args, **kwargs):
        self.total_poin = (self.botol_besar*100) + (self.botol_sedang*70) + (self.botol_kecil*50) + (self.gelas_besar*40) + (self.gelas_kecil*20)
        
        if not self.custom_id:
            prefix = 'S{}'.format(timezone.now().strftime('%y%m%d'))
            prev_instances = self.__class__.objects.filter(custom_id__contains=prefix)
            if prev_instances.exists():
                last_instance_id = prev_instances.last().custom_id[-4:]
                self.custom_id = prefix+'{0:04d}'.format(int(last_instance_id)+1)
            else:
                self.custom_id = prefix+'{0:04d}'.format(1)
        
        if self.is_confirmed:  
            self.transaksi.status = "Selesai"
            
            self.pengguna.poin += self.total_poin
            print("Poin Pengguna Ditambahkan")
            total_penukaran = self.botol_besar + self.botol_sedang + self.botol_kecil + self.gelas_besar + self.gelas_kecil
            self.pengguna.jumlahPenukaran += total_penukaran

            self.warung.jumlah_satuan_pending += total_penukaran
            self.warung.poin += self.total_poin

            print("Total Penukaran Pengguna dan Warung Ditambahkan")
            self.transaksi.total_poin = self.total_poin
            
            
            transaksi_warung = Transaction.objects.create(type="Satuan", warung=self.warung, total_poin=(self.total_poin*10/100), status=TRANSACTION_STATUS_CHOICES[2][0])
            self.transaksi_warung = transaksi_warung
            
            self.transaksi_warung.save()
            self.transaksi.save()
            self.pengguna.save()
            self.warung.save()
        super(ExchangeSatuan, self).save(*args, **kwargs)

class ExchangePaketan(models.Model):
    custom_id = models.CharField(max_length=255, primary_key=True)
    pengguna = models.ForeignKey(to=Pengguna, default=None, blank=True, null=True, on_delete=models.CASCADE)
    transaksi = models.ForeignKey(to=Transaction, default=None, blank=True, null=True, on_delete=models.CASCADE)
    warung = models.ForeignKey(to=Warung, null=True, default=None, blank=True, on_delete=models.CASCADE)
    transaksi_warung = models.ForeignKey(to=Transaction, default=None, blank=True, null=True, on_delete=models.CASCADE,related_name='transaksi_warung_paketan')
    botol_besar = models.IntegerField(default=0, blank=False)
    botol_sedang = models.IntegerField(default=0, blank=False) 
    botol_kecil = models.IntegerField(default=0, blank=False)
    gelas_besar = models.IntegerField(default=0, blank=False)
    gelas_kecil = models.IntegerField(default=0, blank=False)
    total_poin = models.IntegerField(default=0, blank=False)
    date = models.DateTimeField(auto_now_add=True,editable=False)
    is_confirmed = models.BooleanField(default=False)

    def __str__(self):
        return str(self.custom_id)

    def save(self,*args, **kwargs):
        self.total_poin = (self.botol_besar*100) + (self.botol_sedang*70) + (self.botol_kecil*50) + (self.gelas_besar*40) + (self.gelas_kecil*20)
        
        if self.is_confirmed:
            self.transaksi.status = "Selesai"
            
            self.pengguna.poin += self.total_poin

            total_penukaran = self.botol_besar + self.botol_sedang + self.botol_kecil + self.gelas_besar + self.gelas_kecil
            self.pengguna.jumlahPenukaran += total_penukaran

            self.warung.poin += self.total_poin
            
            self.transaksi.total_poin = self.total_poin
            
            transaksi_warung = Transaction.objects.create(type="Paketan", warung=self.warung, total_poin=(self.total_poin*10/100), status=TRANSACTION_STATUS_CHOICES[2][0])
            self.transaksi_warung = transaksi_warung
            
            self.transaksi_warung.save()
            self.pengguna.save()
            self.transaksi.save()
            self.warung.poin()
            
        super(ExchangePaketan, self).save(*args, **kwargs)

class ExchangePenjemputan(models.Model):
    custom_id = models.CharField(max_length=255, primary_key=True)
    alamat = models.ForeignKey(to=Alamat, default=None, on_delete=models.CASCADE)
    pengguna = models.ForeignKey(to=Pengguna, default=None, blank=True, null=True, on_delete=models.CASCADE)
    transaksi = models.ForeignKey(to=Transaction, default=None, blank=True, null=True, on_delete=models.CASCADE)
    botol = models.FloatField(default=0.0, blank=False)
    gelas = models.FloatField(default=0.0, blank=False)
    date = models.DateTimeField(default=timezone.now,editable=True)
    is_confirmed = models.BooleanField(default=False)

    def __str__(self):
        return str(self.custom_id)

    def save(self,*args, **kwargs):
        self.total_poin = (self.botol*5000) + (self.gelas*6000)
        
        if not self.custom_id:
            prefix = 'P{}'.format(timezone.now().strftime('%y%m%d'))
            prev_instances = self.__class__.objects.filter(custom_id__contains=prefix)
            if prev_instances.exists():
                last_instance_id = prev_instances.last().custom_id[-4:]
                self.custom_id = prefix+'{0:04d}'.format(int(last_instance_id)+1)
            else:
                self.custom_id = prefix+'{0:04d}'.format(1)
                
        if self.is_confirmed:
            self.transaksi.status = "Selesai"
            self.pengguna.poin += self.total_poin
            self.transaksi.total_poin = self.total_poin
            self.pengguna.save()
            self.transaksi.save()
            
        super(ExchangePenjemputan, self).save(*args, **kwargs)