from pyexpat import model
from rest_framework import serializers
from .models import ExchangePaketan, ExchangePenjemputan, Transaction, ExchangeSatuan


class ExchangeSatuanSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExchangeSatuan
        fields = ["pengguna_id", "transaksi_id", "warung_id", "botol_besar", "botol_sedang", "botol_kecil", "gelas_besar", "gelas_kecil", "date", "total_poin","is_confirmed"]
        
class ExchangePaketanSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExchangePaketan
        fields = ["custom_id","pengguna_id", "transaksi_id", "warung_id", "botol_besar", "botol_sedang", "botol_kecil", "gelas_besar", "gelas_kecil", "date", "total_poin","is_confirmed"]
        
class ExchangePenjemputanSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExchangePenjemputan
        fields = ["pengguna_id","alamat_id", "transaksi_id","botol", "gelas", "date","is_confirmed"]
        
class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ["custom_id", "total_poin", "status", "type", "date"]
        
        
class ExchangeSatuanPendingSerializer(serializers.ModelSerializer):
    name_pengguna = serializers.SerializerMethodField('get_pengguna_warung')
    
    def get_pengguna_warung(self, exchange_satuan):
        name = exchange_satuan.pengguna.name
        return name
    
    class Meta:
        model = ExchangeSatuan
        fields = ["name_pengguna", "custom_id","transaksi_id", "warung", "botol_besar", "botol_sedang", "botol_kecil", "gelas_besar", "gelas_kecil", "date", "total_poin","is_confirmed"]
        
    