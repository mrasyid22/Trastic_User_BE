from django.contrib import admin

from .models import Achievement, Stage1Achievement, Stage2Achievement, Stage3Achievement, Stage4Achievement
admin.site.register(Achievement)
admin.site.register(Stage1Achievement)
admin.site.register(Stage2Achievement)
admin.site.register(Stage3Achievement)
admin.site.register(Stage4Achievement)

