# Generated by Django 3.1.7 on 2022-05-01 14:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('achievement', '0003_auto_20220430_1731'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stage1achievement',
            name='reward',
            field=models.IntegerField(default=300),
        ),
        migrations.AlterField(
            model_name='stage2achievement',
            name='reward',
            field=models.IntegerField(default=2000),
        ),
        migrations.AlterField(
            model_name='stage3achievement',
            name='reward',
            field=models.IntegerField(default=5000),
        ),
        migrations.AlterField(
            model_name='stage4achievement',
            name='reward',
            field=models.IntegerField(default=10000),
        ),
    ]
