# Generated by Django 3.1.7 on 2022-04-30 17:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('achievement', '0002_remove_stage1achievement_pengguna'),
    ]

    operations = [
        migrations.RenameField(
            model_name='stage1achievement',
            old_name='is_achived',
            new_name='is_achieved',
        ),
        migrations.RenameField(
            model_name='stage2achievement',
            old_name='is_achived',
            new_name='is_achieved',
        ),
        migrations.RenameField(
            model_name='stage3achievement',
            old_name='is_achived',
            new_name='is_achieved',
        ),
        migrations.RenameField(
            model_name='stage4achievement',
            old_name='is_achived',
            new_name='is_achieved',
        ),
    ]
