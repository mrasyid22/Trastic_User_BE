from django.shortcuts import render
from authentication.models import Pengguna, Warung, User
from exchange import permissions
from .serializers import AchievementsSerializer, Stage1Serializer, Stage2Serializer, Stage3Serializer, Stage4Serializer
from .models import Achievement, Stage1Achievement, Stage2Achievement, Stage3Achievement, Stage4Achievement
from rest_framework.generics import ListAPIView, UpdateAPIView
from .permissions import IsPengguna
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework import generics, status

class ListAchievementsView(ListAPIView):
    serializer_class = AchievementsSerializer

    permission_classes = (permissions.IsAuthenticated, IsPengguna)
    
    def get_queryset(self):
        achievement = Achievement.objects.all()
        return achievement

class ConfirmStage1AchievementView(UpdateAPIView):
    serializer_class = Stage1Serializer
    permission_classes = (permissions.IsAuthenticated, IsPengguna)

    def get_object(self):
        pengguna = Pengguna.objects.get(user=self.request.user)
        return Achievement.objects.get(pengguna=pengguna).stage1
    
    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        
        serializer = self.get_serializer(instance,data=request.data,partial=True)
        if instance.is_achieved:
            if not instance.is_claimed:
                if serializer.is_valid():
                    serializer.save()
                    print("masuk mau claim")
                    return Response({'message': "Reward berhasil diklaim"}, status=status.HTTP_200_OK)
            else:
                return Response({'message': "Reward sudah pernah diklaim"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'message': "Reward belum dapat diklaim"}, status=status.HTTP_400_BAD_REQUEST)
        
class ConfirmStage2AchievementView(UpdateAPIView):
    serializer_class = Stage2Serializer
    permission_classes = (permissions.IsAuthenticated, IsPengguna)
    
    def get_object(self):
        pengguna = Pengguna.objects.get(user=self.request.user)
        return Achievement.objects.get(pengguna=pengguna).stage2
    
    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        
        serializer = self.get_serializer(instance,data=request.data,partial=True)
        if instance.is_achieved:
            if not instance.is_claimed:
                if serializer.is_valid():
                    serializer.save()
                    return Response({'message': "Reward berhasil diklaim"}, status=status.HTTP_200_OK)
            else:
                return Response({'message': "Reward sudah pernah diklaim"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'message': "Reward belum dapat diklaim"}, status=status.HTTP_400_BAD_REQUEST)
        
class ConfirmStage3AchievementView(UpdateAPIView):
    serializer_class = Stage3Serializer
    permission_classes = (permissions.IsAuthenticated, IsPengguna)
    
    def get_object(self):
        pengguna = Pengguna.objects.get(user=self.request.user)
        return Achievement.objects.get(pengguna=pengguna).stage3
    
    def update(self, request, *args, **kwargs):
        instance = self.get_object()
    
        serializer = self.get_serializer(instance,data=request.data,partial=True)
        if instance.is_achieved:
            if not instance.is_claimed:
                if serializer.is_valid():
                    serializer.save()
                    return Response({'message': "Reward berhasil diklaim"}, status=status.HTTP_200_OK)
            else:
                return Response({'message': "Reward sudah pernah diklaim"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'message': "Reward belum dapat diklaim"}, status=status.HTTP_400_BAD_REQUEST)
        
class ConfirmStage4AchievementView(UpdateAPIView):
    serializer_class = Stage4Serializer
    permission_classes = (permissions.IsAuthenticated, IsPengguna)
    
    def get_object(self):
        pengguna = Pengguna.objects.get(user=self.request.user)
        return Achievement.objects.get(pengguna=pengguna).stage4
     
    def update(self, request, *args, **kwargs):
        instance = self.get_object()
    
        serializer = self.get_serializer(instance,data=request.data,partial=True)
        if instance.is_achieved:
            if not instance.is_claimed:
                if serializer.is_valid():
                    serializer.save()
                    return Response({'message': "Reward berhasil diklaim"}, status=status.HTTP_200_OK)
            else:
                return Response({'message': "Reward sudah pernah diklaim"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'message': "Reward belum dapat diklaim"}, status=status.HTTP_400_BAD_REQUEST)