from ctypes import addressof
from django.db import models
from authentication.models import Pengguna
from exchange.models import Transaction
import random
from django.utils import timezone
from datetime import datetime

class Stage1Achievement(models.Model):
    reward = models.IntegerField(default=300, blank=False)
    is_achieved = models.BooleanField(default=False)
    is_claimed = models.BooleanField(default=False)
    
    def save(self, *args, **kwargs):
        print('hehe')
        if self.is_claimed:
            print("mau di save")
            achievement = Achievement.objects.get(stage1=self)
            pengguna = achievement.pengguna
            transaksi = Transaction.objects.create(pengguna=pengguna, total_poin=self.reward, status="Selesai", type="Reward")
            pengguna.poin += self.reward
            pengguna.save()
            self.reward = 0
        super(Stage1Achievement, self).save(*args, **kwargs) 

class Stage2Achievement(models.Model):
    reward = models.IntegerField(default=2000, blank=False)
    is_achieved = models.BooleanField(default=False)
    is_claimed = models.BooleanField(default=False)
    
    def save(self, *args, **kwargs):
        if self.is_claimed:
            achievement = Achievement.objects.get(stage1=self)
            pengguna = achievement.pengguna
            transaksi = Transaction.objects.create(pengguna=pengguna, total_poin=self.reward, status="Selesai", type="Reward")
            pengguna.poin += self.reward
            pengguna.save()
            self.reward = 0
        super(Stage2Achievement, self).save(*args, **kwargs) 

class Stage3Achievement(models.Model):
    reward = models.IntegerField(default=5000, blank=False)
    is_achieved = models.BooleanField(default=False)
    is_claimed = models.BooleanField(default=False)
    
    def save(self, *args, **kwargs):
        if self.is_claimed:
            achievement = Achievement.objects.get(stage1=self)
            pengguna = achievement.pengguna
            transaksi = Transaction.objects.create(pengguna=pengguna, total_poin=self.reward, status="Selesai", type="Reward")
            pengguna.poin += self.reward
            pengguna.save()
            self.reward = 0
        super(Stage3Achievement, self).save(*args, **kwargs) 

class Stage4Achievement(models.Model):
    reward = models.IntegerField(default=10000, blank=False)
    is_achieved = models.BooleanField(default=False)
    is_claimed = models.BooleanField(default=False)
    
    def save(self, *args, **kwargs):
        if self.is_claimed:
            achievement = Achievement.objects.get(stage1=self)
            pengguna = achievement.pengguna
            transaksi = Transaction.objects.create(pengguna=pengguna, total_poin=self.reward, status="Selesai", type="Reward")
            pengguna.poin += self.reward
            pengguna.save()
            self.reward = 0
        super(Stage4Achievement, self).save(*args, **kwargs) 

class Achievement(models.Model):
    pengguna = models.OneToOneField(to=Pengguna, default=None, blank=True, null=True, on_delete=models.CASCADE)
    stage1 = models.OneToOneField(Stage1Achievement, default=None, blank=True, null=True, on_delete=models.CASCADE)
    stage2 = models.OneToOneField(Stage2Achievement, default=None, blank=True, null=True, on_delete=models.CASCADE)
    stage3 = models.OneToOneField(Stage3Achievement, default=None, blank=True, null=True, on_delete=models.CASCADE)
    stage4 = models.OneToOneField(Stage4Achievement, default=None, blank=True, null=True, on_delete=models.CASCADE)
    
    def __str__(self):
        return str(self.pengguna.name)
    
    def check_status(self):
        if self.pengguna.jumlahPenukaran >= 30 and self.pengguna.jumlahPenukaran <200:
            self.stage1.is_achieved = True
            self.stage1.save()
            
        if self.pengguna.jumlahPenukaran >= 200 and self.pengguna.jumlahPenukaran < 500:

            self.stage2.is_achieved = True
            self.stage2.save()
        if self.pengguna.jumlahPenukaran >= 500 and self.pengguna.jumlahPenukaran < 1000:

            self.stage3.is_achieved = True
            self.stage3.save()
            
        if self.pengguna.jumlahPenukaran >= 1000:
            self.stage4.is_achieved = True
            self.stage4.save()

